/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.modelo;

/**
 *
 * @author Matico
 */
public class Interes {
    public static double calcular(String capital, String tasa, String periodo) {
        double c = Double.valueOf(capital);
        double i = Double.valueOf(tasa);
        double t = Double.valueOf(periodo);
        
        return c * (i / 100) * t;
    }
}
