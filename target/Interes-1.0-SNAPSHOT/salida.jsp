<%-- 
    Document   : salida
    Created on : Apr 5, 2020, 4:33:49 PM
    Author     : Matico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interés Simple</title>
    </head>
    <body>
        <%
            String capital = (String) request.getAttribute("capital");
            String tasa = (String) request.getAttribute("tasa");
            String periodo = (String) request.getAttribute("periodo");
            double interes = (double) request.getAttribute("interes");
        %>
        
        <h1>Resultado</h1>
        
        <p>El interés simple de $<%=capital%>, con una tasa anual de <%=tasa%>% y por un periodo de <%=periodo%> años es:</p>
        <h3>$<%=interes%></h3>
        
        <a href="/Interes-1.0-SNAPSHOT">Nuevo cálculo</a>
    </body>
</html>
