<%--
    Document : index
    Created on : Apr 5, 2020, 3:27:24 PM
    Author : Matico
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Interés Simple</title>
        <style>
            .campo {
                margin-bottom: 1em;
            }
            
            .campo * {
                display: block;
            }
            
            #submit {
                margin-top: 1em;
            } 
        </style>
    </head>
    <body>
        <h1>Calculadora de interés simple con periodos anuales</h1>

        <form action="controlador">
            <div class="campo">
                <label for="capital">Capital:</label>
                <input type="number" name="capital" />
            </div>
            
            <div class="campo">
                <label for="tasa">Tasa de interés anual:</label>
                <input type="number" name="tasa" />
            </div>

            <div class="campo">
                <label for="periodo">Periodo anual:</label>
                <input type="number" name="periodo" />
            </div>
            
            <input type="submit" id="submit" value="Calcular" />
        </form>
    </body>
</html>
